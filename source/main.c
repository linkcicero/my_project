#include <stdio.h>
#include "dice.h"

int main () {
    // create a seed for the ramdom function used in the rollDice()
    initializeSeed();
    // variable declaration
    int faces;
    // choose the number of faces
    printf("How many faces will your dice have? ");
    scanf("%d", &faces);
    // generate a ramdom number from 1 to faces, as if it were a non-addicted dice
    printf("Let's roll the dice: %d\n", rollDice(faces));
    return 0;
}
